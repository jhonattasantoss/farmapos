<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    return [
        'name' => $faker->name,
        'login' => $faker->firstName,
        'cpf' => $faker->randomNumber(5) . $faker->randomNumber(6),
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Componente::class, function (Faker\Generator $faker) {
    return [
        'descricao' => $faker->word,
        'unidade' => $faker->randomElement(['mg','g','ml'])
    ];
});

$factory->define(App\HistoricoPreco::class, function (Faker\Generator $faker) {
    return [
        'preco' => $faker->randomFloat(2,0.5,5),
        'ativo' => $faker->randomElement([true, false])
    ];
});

$factory->define(App\Formula::class, function (Faker\Generator $faker) {
    return [
        'tipo' => $faker->randomElement(['capsula', 'envelope','pastila']),
        'quantidade' => $faker->randomNumber(1,10),
        'status' => $faker->randomElement(['incompleta', 'pronta']),
        'volume' => $faker->randomElement([30,40, 60, 90,120,200]),
        'desconto' => $faker->randomFloat(0.0,0.99)
    ];
});

$factory->define(App\Orcamento::class, function (Faker\Generator $faker) {
    return [
        'status' => $faker->randomElement(['incompleto', 'pronto']),
    ];
});
