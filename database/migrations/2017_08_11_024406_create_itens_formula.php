<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItensFormula extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('itens_formula', function (Blueprint $table) {
            $table->integer('componente_id')->unsigned();
            $table->integer('formula_id')->unsigned();
            $table->foreign('componente_id')->references('id')->on('componentes');
            $table->foreign('formula_id')->references('id')->on('formulas');
            $table->decimal('preco');
            $table->double('concentracao');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('itens_formula');
    }
}
