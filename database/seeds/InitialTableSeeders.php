<?php

use Illuminate\Database\Seeder;

class InitialTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\User::class,50)->create();

        factory(\App\Componente::class,50)->create()->each(function ($c) {
            $c->precos()->save(factory(App\HistoricoPreco::class)->make());
        });

    }
}
