@extends('layouts.app')

@section('header')
    <header id="header">
        <h1>Novo Orçamento</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Orçamentos</li>
        </ol>
    </header>
@endsection

@section('body')
    <div class="row">

        <div class="col-xs-12">
            <div class="card">
                <form method="post" action="{{ route('formulas.store') }}">
                    {{ csrf_field() }}
                <header class="card-heading ">
                    <h2 class="card-title">Adicionar Formulas</h2>
                    <small>Escolha o Cliente</small>

                    <ul class="card-actions icons right-top">
                        <li>
                            <button class="btn btn-primary" type="submit">Finalizar</button>
                        </li>
                    </ul>
                </header>
                <div class="card-body">
                    <button type="button" class="btn btn-info btn-block" data-toggle="modal" data-target="#basic_modal">Adicionar Formula</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Modal Demos ===========================================================-->
    <div class="modal fade" id="basic_modal" tabindex="-1" role="dialog" aria-labelledby="basic_modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title" id="myModalLabel-2">Adicionar Ativos</h4>
                    <ul class="card-actions icons right-top">

                        <a href="javascript:void(0)" data-dismiss="modal" class="text-white" aria-label="Close">
                            <i class="zmdi zmdi-close"></i>
                        </a>
                        </li>
                    </ul>
                </div>
                <div class="modal-body">
                    <form action="{{ route('formulas.store') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="orcamentoId" value="{{$orcamentoId }}">
                        <div class="tabpanel">
                            <ul class="nav nav-tabs p-0">
                                <li class="active" role="presentation"><a href="#tab-1" data-toggle="tab" aria-expanded="true">Tab 1</a></li>
                                <li role="presentation"><a href="#tab-2" data-toggle="tab" aria-expanded="true">Tab 2</a></li>
                            </ul>
                        </div>
                        <div class="tab-content  p-20">
                            <div class="tab-pane fadeIn active" id="tab-1">
                                <div class="form-group is-empty">
                                    <label for="" class="control-label">Tipo:</label>
                                    <select class="select form-control" name="tipo">
                                        <option value="capsula">CAPSULA</option>
                                        <option value="pastila">PASTILA</option>
                                        <option value="envelope">ENVELOPE</option>
                                    </select>
                                </div>

                                <div class="form-group is-empty">
                                    <label for="" class="control-label">Volume:</label>
                                    <select class="select form-control" name="volume">
                                        <option value="30">30</option>
                                        <option value="60">60</option>
                                        <option value="120">120</option>
                                        <option value="200">200</option>
                                    </select>
                                </div>

                                <div class="form-group is-empty">
                                    <label for="quantidade" class="control-label">Quantidade</label>
                                    <input type="number" class="form-control" name="quantidade" id="quantidade" placeholder="Quantidade">
                                </div>
                            </div>
                            <div class="tab-pane fadeIn" id="tab-2">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>#</th>
                                            <th>Nome</th>
                                            <th>Unidade</th>
                                            <th>Preço</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($componentes as $key=>$componente)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>
                                                <div class="checkbox">
                                                    <label>
                                                        <input value="{{ $componente->id }}" name="componentes[]" type="checkbox">
                                                    </label>
                                                </div>
                                            </td>
                                            <td>
                                                {{ $componente->descricao }}
                                            </td>
                                            <td>
                                                {{ $componente->unidade }}
                                            </td>
                                            <td>
                                                R$ {{ $componente->precos[0]->preco }}
                                            </td>
                                        </tr>
                                            @endforeach
                                    </tbody>
                                </table>

                            </div>
                        </div>


                        <button type="submit" class="btn btn-primary">Gravar</button>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-primary">Ok</button>
                </div>
            </div>
            <!-- modal-content -->
        </div>
        <!-- modal-dialog -->
    </div>
@endsection


