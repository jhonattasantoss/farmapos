@extends('layouts.app')

@section('header')
    <header id="header">
        <h1>Novo Orçamento</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Orçamentos</li>
        </ol>
    </header>
@endsection

@section('body')
    <div class="row">

        <div class="col-xs-12">
            <div class="card">
                <form method="post" action="{{ route('orcamentos.store') }}">
                    {{ csrf_field() }}
                <header class="card-heading ">
                    <h2 class="card-title">Adicionar o Cliente</h2>
                    <small>Escolha o Cliente</small>

                    <ul class="card-actions icons right-top">
                        <li>
                            <button class="btn btn-primary" type="submit">Adicionar Formulas</button>
                        </li>
                    </ul>
                </header>
                <div class="card-body">
                        @include('orcamento._form')
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
