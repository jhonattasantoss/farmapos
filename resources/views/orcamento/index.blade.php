@extends('layouts.app')

@section('header')
    <header id="header">
        <h1>Orçamentos</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">Orçamentos</li>
        </ol>
    </header>
@endsection

@section('body')
    <div class="row">

        <div class="col-xs-12">
            <div class="card">
                <header class="card-heading ">
                    <h2 class="card-title">Card Header</h2>
                    <small>Paleo flexitarian bushwick letterpress</small>
                </header>
                <div class="card-body">
                    <a class="btn btn-primary" href="{{route('orcamentos.create')}}">Novo Orcamento</a>
                    lista Orçamentos
                </div>
            </div>
        </div>
    </div>
@endsection