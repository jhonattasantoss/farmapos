<aside id="app_sidebar-left">
    <div id="logo_wrapper">
        <ul>
            <li class="logo-icon">
                <a href="{{ route('home') }}">
                    <div class="logo">
                        {{--<img src="assets/img/logo/logo-icon.png" alt="Logo">--}}
                    </div>
                    <h1 class="brand-text">{{ config('app.name', 'Azevedo Bastos') }}</h1>
                </a>
            </li>
            <li class="menu-icon">
                <a href="javascript:void(0)" role="button" data-toggle-state="app_sidebar-menu-collapsed" data-key="leftSideBar">
                    <i class="mdi mdi-backburger"></i>
                </a>
            </li>
        </ul>
    </div>
    <nav id="app_main-menu-wrapper" class="scrollbar">
        <div class="sidebar-inner sidebar-push">
            <ul class="nav nav-pills nav-stacked">
                <li class="sidebar-header">NAVIGATION</li>
                <li class="{{ route_state('home') }}"><a href="{{ route('home') }}"><i class="zmdi zmdi-view-dashboard"></i>Inicio</a></li>
                <li class="nav-dropdown {{ route_state(['nascimento.*','casamento.*']) }}">
                    <a href="#"><i class="zmdi zmdi-view-quilt"></i>Certidão</a>
                    <ul class="nav-sub">
                        <li class="nav-dropdown">
                            <a href="#">Nascimento </a>
                            <ul class="nav-sub">
                                <li><a href="#">Novo registro</a></li>
                                <li><a href="#">Registro em Andamento</a></li>
                                <li><a href="{{ route('nascimento.index') }}">Busca Registro</a></li>
                                <li><a href="#">Importar Registro</a></li>
                            </ul>
                        </li>
                        <li class="nav-dropdown">
                            <a href="#">Casamento</a>
                            <ul class="nav-sub">
                                <li><a href="#">Novo Registro</a></li>
                                <li><a href="#">Registro em Andamento</a></li>
                                <li><a href="#">Aguardando Retirada</a></li>
                                <li><a href="#">Busca Registro</a></li>
                            </ul>
                        </li>
                        <li class="nav-dropdown">
                            <a href="#">Obito</a>
                            <ul class="nav-sub">
                                <li><a href="#">Novo Registro</a></li>
                                <li><a href="#">Busca Registro</a></li>

                            </ul>
                        </li>
                    </ul>
                </li>
                <li class="nav-dropdown {{ route_state('solicitacao.*') }}"><a href="#"><i class="zmdi zmdi-widgets"></i>Segunda Via</a>
                    <ul class="nav-sub">
                        <li><a href="#">Solicitar</a></li>
                        <li><a href="{{ route('solicitacao.index') }}">Solicitações Pendentes</a></li>
                        <li><a href="#">Aguardando retirada</a></li>
                        <li><a href="#">Solicitações Concluidas</a></li>
                    </ul>
                </li>
                <li class="nav-dropdown  {{ route_state('anotacao.*') }}"><a href="#"><i class="zmdi zmdi-chart"></i>Anotações </a>
                    <ul class="nav-sub">
                        <li><a href="#">Nova Anotação</a></li>
                        <li><a href="{{ route('anotacao.index') }}">Indice</a></li>
                    </ul>
                </li>
                <li class="nav-dropdown {{ route_state('reconhecimento-firma.*') }}"><a href="#"><i class="zmdi zmdi-chart"></i>Reconhecimento de Firma</a>
                    <ul class="nav-sub">
                        <li><a href="{{ route('reconhecimento-firma.index') }}">Reconhecer Firma</a></li>
                        <li><a href="#">Reconhecimento Mensalista</a></li>
                        <li><a href="#">Novo Cliente</a></li>
                        <li><a href="#">Buscar</a></li>
                        <li><a href="#">Relatório</a></li>
                    </ul>
                </li>
                <li class="nav-dropdown  {{ route_state('autenticacao.*') }}"><a href="#"><i class="zmdi zmdi-chart"></i>Autenticação</a>
                    <ul class="nav-sub">
                        <li><a href="#">Nova Autenticação</a></li>
                        <li><a href="#">Nova Autenticação + copia</a></li>
                        <li><a href="#">Cópias</a></li>
                        <li><a href="#">Relatorios</a></li>
                        <li><a href="#">Solicitações Autenticação Digital</a></li>
                        <li><a href="#">Nova Autenticação Digital</a></li>
                        <li><a href="#">Relatorio</a></li>
                    </ul>
                </li>
                <li class="nav-dropdown {{ route_state('mensalista.*') }}"><a href="#"><i class="zmdi zmdi-chart"></i>Cliente PJ</a>
                    <ul class="nav-sub">
                        <li><a href="{{ route('mensalista.index') }}">Clientes</a></li>
                        <li><a href="{{ route('mensalista.create') }}">Novo Cliente</a></li>
                        <li><a href="#">Relatorio Mensalista</a></li>
                    </ul>
                </li>
                <li class="nav-dropdown {{ route_state('autenticacao.*') }}"><a href="#"><i class="zmdi zmdi-chart"></i>Caixa</a>
                    <ul class="nav-sub">
                        <li><a href="#">Autenticação Digital</a></li>
                        <li><a href="#">Autenticação Digital Mensalista</a></li>
                        <li><a href="#">Autenticação Digital Fortaleza</a></li>
                        <li><a href="#">Caixa</a></li>
                    </ul>
                </li>
                <li class="sidebar-header">Ferramenta</li>

                <li class="{{ route_state('texto.*') }}"><a href="#"><i class="zmdi zmdi-folder"></i>Novo Texto</a></li>
                <li class="{{ route_state('atos.*') }}"><a href="#"><i class="zmdi zmdi-view-column"></i>Tipos de Atos</a></li>
                <li class="{{ route_state('atos.*') }}"><a href="#"><i class="mdi mdi-lightbulb"></i>Atos Simples</a></li>
                <li class="{{ route_state('atos.*') }}"><a href="#"><i class="zmdi zmdi-email"></i>Atos Compostos</a></li>

                <li class="sidebar-header">Usuario</li>
                <li><a href="#"><i class="zmdi zmdi-account"></i>Alterar dados</a></li>

                <li class="sidebar-header">Permissões</li>
                <li class="{{ route_state('cargos') }}"><a href="{{ route('cargos') }}"><i class="zmdi zmdi-folder"></i>Cargos</a></li>
                <li class="{{ route_state('permissao') }}"><a href="{{ route('permissao') }}"><i class="zmdi zmdi-folder"></i>Permissões</a></li>
            </ul>
        </div>
    </nav>
</aside>