<header id="app_topnavbar-wrapper">
    <div class="container-fluid">
        <nav role="navigation" class="navbar topnavbar">
            <div class="nav-wrapper">

                <div class="navbar-header">
                    <a href="{{ route('home') }}">
                        <h1 class="brand-text md-text-black-50">{{ config('app.name', 'AppFarma') }}</h1>
                    </a>
                </div>

                @if (Auth::user())
                    <ul class="nav navbar-nav left-menu">
                        <li><a href="{{ route('orcamentos.index') }}">Orçamentos</a></li>
                        <li><a href="app-mail.html">Pedidos</a></li>
                        <li><a href="app-mail.html">Relatorios</a></li>
                    </ul>

                    <ul class="nav navbar-nav pull-right">
                        <li class="dropdown avatar-menu">
                            <a href="javascript:void(0)" data-toggle="dropdown" aria-expanded="false">
								<span class="meta">
									<span class="avatar">
										<i class="zmdi zmdi-account"></i>
										<i class="badge mini success status"></i>
									</span>
									<span class="name"> {{ Auth::user()->name }}</span>
									<span class="caret"></span>
								</span>
                            </a>
                            <ul class="dropdown-menu btn-primary dropdown-menu-right">
                                <li>
                                    <a href="page-profile.html"><i class="zmdi zmdi-account"></i> Profile</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><i class="zmdi zmdi-settings"></i> Account Settings</a>
                                </li>
                                <li>
                                    <a href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        <i class="zmdi zmdi-sign-in"></i> Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                @else
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    </ul>
                @endif

            </div>
        </nav>
    </div>
</header>