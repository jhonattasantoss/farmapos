<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('theme/css/vendor.bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/app.bundle.css') }}" rel="stylesheet">
    <link href="{{ asset('theme/css/theme-a.css') }}" rel="stylesheet">
</head>
<body>
<div id="app_wrapper">

    <header id="app_topnavbar-wrapper">
        <div class="container-fluid">
            <nav role="navigation" class="navbar topnavbar">
                <div class="nav-wrapper">

                    <div class="navbar-header">
                        <a href="{{ route('home') }}">
                            <h1 class="brand-text md-text-black-50">{{ config('app.name', 'AppFarma') }}</h1>
                        </a>
                    </div>

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="{{ route('login') }}">Login</a></li>
                        <li><a href="{{ route('register') }}">Register</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>


    <section id="content_outer_wrapper" style="padding-left: 0 !important;">
        <div id="content_wrapper" class="card-overlay">
            <div id="header_wrapper" class="header-md">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            @yield('header')
                        </div>
                    </div>
                </div>
            </div>

            <div id="content" class="container-fluid">

                @yield('body')

            </div>
        </div>

    </section>
</div>


{{--<script src="{{ asset('js/app.js') }}"></script>--}}
<script src="{{ asset('theme/js/vendor.bundle.js') }}"></script>
<script src="{{ asset('theme/js/app.bundle.js') }}"></script>
</body>
</html>
