@extends('layouts.app')

@section('header')
    <header id="header">
        <h1>Pagina Inicial</h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}">Home</a></li>
            <li class="active">App Farma</li>
        </ol>
    </header>
@endsection

@section('body')
    <div class="row">

        <div class="col-xs-12">
            <div class="card">
                <header class="card-heading ">
                    <h2 class="card-title">Card Header</h2>
                    <small>Paleo flexitarian bushwick letterpress</small>
                    <ul class="card-actions icons right-top">
                        <li class="dropdown">
                            <a href="javascript:void(0)" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>
                            <ul class="dropdown-menu btn-primary dropdown-menu-right">
                                <li>
                                    <a href="javascript:void(0)">Option One</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Option Two</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Option Three</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </header>
                <div class="card-body">

                </div>
            </div>
        </div>

        <div class="col-lg-4">
            <div class="card">
                <header class="card-heading border-bottom">
                    <h2 class="card-title">Nascimentos</h2>
                    <small>Certidões Pendentes</small>
                    <ul class="card-actions icons right-top">
                        <li>
                            <a href="javascript:void(0)" data-toggle="refresh">
                                <i class="zmdi zmdi-refresh-alt"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:void(0)" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>
                            <ul class="dropdown-menu btn-primary dropdown-menu-right">
                                <li>
                                    <a href="javascript:void(0)">Option One</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Option Two</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Option Three</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </header>
                <div class="card-body p-0">
                    <ul class="list-group ">
                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>


                    </ul>
                </div>
                <div class="card-footer border-top">
                    <ul class="more">
                        <li>
                            <a href="javascript:void(0)">Ver Mais <span class="label label-warning">10</span></a>
                        </li>
                    </ul>
                    <ul class="card-actions icons right">
                        <li>
                            <button class="btn btn-primary btn-fab"><i class="zmdi zmdi-plus"></i></button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <header class="card-heading border-bottom">
                    <h2 class="card-title">Casamentos</h2>
                    <small>Certidões Pendentes</small>
                    <ul class="card-actions icons right-top">
                        <li>
                            <a href="javascript:void(0)" data-toggle="refresh">
                                <i class="zmdi zmdi-refresh-alt"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:void(0)" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>
                            <ul class="dropdown-menu btn-primary dropdown-menu-right">
                                <li>
                                    <a href="javascript:void(0)">Option One</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Option Two</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Option Three</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </header>
                <div class="card-body p-0">
                    <ul class="list-group ">
                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>


                    </ul>
                </div>
                <div class="card-footer border-top">
                    <ul class="more">
                        <li>
                            <a href="javascript:void(0)">Ver Mais <span class="label label-warning">10</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <div class="card">
                <header class="card-heading border-bottom">
                    <h2 class="card-title">Óbitos</h2>
                    <small>Certidões Pendentes</small>
                    <ul class="card-actions icons right-top">
                        <li>
                            <a href="javascript:void(0)" data-toggle="refresh">
                                <i class="zmdi zmdi-refresh-alt"></i>
                            </a>
                        </li>
                        <li class="dropdown">
                            <a href="javascript:void(0)" data-toggle="dropdown">
                                <i class="zmdi zmdi-more-vert"></i>
                            </a>
                            <ul class="dropdown-menu btn-primary dropdown-menu-right">
                                <li>
                                    <a href="javascript:void(0)">Option One</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Option Two</a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)">Option Three</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </header>
                <div class="card-body p-0">
                    <ul class="list-group ">
                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>

                        <li class="list-group-item ">
                            {{--<span class="pull-left"><img src="{{asset('theme/img/profiles/07.jpg')}}" alt="" class="img-circle max-w-40 m-r-10 "></span>--}}
                            <span class="pull-left zmdi zmdi-circle zmdi-hc-3x c-green"></span>
                            <div class="list-group-item-body">
                                <div class="list-group-item-heading">Tile with avatar</div>
                                <div class="list-group-item-text">bushwick man bun pok pok shoreditch</div>
                            </div>
                        </li>


                    </ul>
                </div>
                <div class="card-footer border-top">
                    <ul class="more">
                        <li>
                            <a href="javascript:void(0)">Ver Mais <span class="label label-warning">10</span></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>


    </div>
@endsection