<?php

namespace App\Http\Controllers;

use App\Componente;
use App\Formula;
use Illuminate\Http\Request;

class FormulaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $orcamentoId = $request->get('orcamentoId');
        $componentes = Componente::with('precos');
        $componentes = $componentes->get()->filter(function ($value, $key) {
            //dd($value->precos);
            return count($value->precos) > 0;
        });;
        return view('formula.create',compact('orcamentoId','componentes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orcamentoId = $request->get('orcamentoId');
        $formula = $request->only('tipo','volume','quantidade');
        $formula['status'] = "pronta";
        $formula['desconto'] = 0.1;
        $formulaSave = Formula::create($formula);

        $formulaSave->componentes()->attach($orcamentoId,$request->get('componentes'));

        return redirect()->route('formulas.create', compact('orcamentoId'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
