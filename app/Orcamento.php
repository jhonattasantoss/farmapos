<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orcamento extends Model
{
    protected $fillable = ['status','cliente_id','vendedor_id'];

    public function vendedor()
    {
        $this->hasOne(User::class);
    }

    public function cliente()
    {
        $this->hasOne(User::class);
    }
}
