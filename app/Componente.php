<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Componente extends Model
{
    public function precos()
    {
        return $this->hasMany(HistoricoPreco::class)->where('ativo', true);
    }

    public function formulas()
    {
        return $this->belongsToMany(Formula::class);
    }
}
