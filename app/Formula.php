<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Formula extends Model
{
    protected $fillable = ['tipo','volume','quantidade','status','desconto'];

    public function componentes()
    {
        return $this->hasMany(Componente::class);
    }
}
